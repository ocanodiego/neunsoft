import React, { useRef } from 'react';
import Image from "next/image";

import Header from "@/components/header"
import Thunders from "@/components/thunders"
import Link from 'next/link';

export default function Home() {

  const thunderContainerRef = useRef<HTMLDivElement>(null);

  const currentYear = new Date().getFullYear();

  return (
  <div className="main bg-black text-white">
    <Header></Header>
    <div className="hero mt-48 md:mt-40 text-center px-10">
      {/* <Thunders></Thunders> */}
      <h1 className="text-4xl font-thin leading-relaxed"> <b>Reinventing</b> Software Development</h1>
      <h2 className="text-4xl my-80 font-thin leading-relaxed">We help you turn your <b>ideas</b> into <b>reality</b>. </h2>
      <h2 className="text-4xl my-80 font-thin leading-relaxed">Wether it is for your <b>business</b> or for a <b>new idea or startup</b>. We have a <b>specialized</b> approach for every situation. </h2>
      <h2 className="text-4xl my-80 font-thin leading-relaxed">For your stablished <b>business</b>: We evaluate and help you polish or create your <b>specification</b>. Next, we negotiate how we will work together. </h2>
      <h2 className="text-4xl my-80 font-thin leading-relaxed">For your <b>startup</b>: We have comprehensive guides with key knowledge that will help you get set up extremely quickly and iterate fast. Read more <Link className='font-bold' href="/startups">here</Link>.</h2>
      <h2 className="text-4xl my-80 font-thin leading-relaxed"></h2>
      <a className="my-80 text-4xl font-thin text-white block cursor-pointer">Portfolio</a>
      {/* <h2 className="mt-4">Find Out Why</h2> */}
    </div>
    <footer className="bottom-2 w-full text-center">
      <div className="footer text-white text-center p-5 font-thin text-sm">
        <p>&copy; {currentYear} NEUN - contact@neunsoft.com</p>
      </div>
    </footer>
  </div>
  );
}
