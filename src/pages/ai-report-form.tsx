import React, { useEffect } from 'react';
import Head from 'next/head';
import useFormStore from '../stores/ai-form.store';
import Question from '../components/ui/question';
import Header from '../components/header';
import CalendlyWidget from '../components/CalendlyWidget';
import axios from 'axios';

const Home: React.FC = () => {
  const { step, formData, updateFormData, nextStep, prevStep } = useFormStore();
  const [formSubmitted, setFormSubmitted] = React.useState(false);
  const [customerId, setCustomerId] = React.useState<string | null>(null);

  const handleChange = (name: string, value: string) => {
    updateFormData({ [name]: value });
  };

  const handleNext = async () => {
    await handleSubmit();
    if (step < totalSteps) {
      nextStep();
    } else {
      setFormSubmitted(true);
    }
  };

  const handlePrev = () => {
    prevStep();
  };

  const handleSubmit = async () => {
    const data = { ...formData };

    try {
      let response;
      if (!customerId) {
        // Call create endpoint
        response = await axios.post('/api/entries', data);
        setCustomerId(response.data.guid);
      } else {
        // Call update endpoint
        response = await axios.put(`/api/entries/${customerId}`, data);
      }
    } catch (error) {
      console.error('Error submitting form:', error);
    }
  };

  useEffect(() => {
    const handleKeyDown = (e: KeyboardEvent) => {
      if (e.key === 'Enter') {
        handleNext();
      }
    };

    document.addEventListener('keydown', handleKeyDown);
    return () => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [step]);

  const totalSteps = 7;
  return (
    <div className="min-h-screen bg-black text-white">
      <Head>
        <title>AI Adoption for Small Businesses</title>
        <meta name="description" content="AI Adoption for Small Businesses" />
      </Head>
      <Header />
      <main className="max-w-3xl mx-auto py-10 bg-black text-white flex items-center">
        {!formSubmitted && (
          <div className="px-5 w-full mt-30 pb-16">
            {step === 0 && (
              <div className="text-center font-thin mt-40">
                <h1 className="text-4xl">Let&#39;s get started!</h1>
                <p className="mt-4 text-2xl">
                  To be able to help you effectively we will need some quick information from your business.
                </p>
              </div>
            )}
            {step === 1 && (
              <div className="mt-40">
                <Question
                  label="What is your business <b>Instagram</b> handle?"
                  name="instagram"
                  type="text"
                  value={formData.instagram}
                  onChange={handleChange}
                  required={false}
                />
              </div>
            )}
            {step === 2 && (
              <div className="mt-40">
                <Question
                  label="What is your business <b>Facebook</b> page?"
                  name="facebook"
                  type="text"
                  value={formData.facebook}
                  onChange={handleChange}
                  required={false}
                />
              </div>
            )}
            {step === 3 && (
              <div className="mt-40">
                <Question
                  label="What is your business <b>LinkedIn</b> page?"
                  name="linkedin"
                  type="text"
                  value={formData.linkedin}
                  onChange={handleChange}
                  required={false}
                />
              </div>
            )}
            {step === 4 && (
              <div className="mt-40">
                <Question
                  label="What is your business <b>website</b>?"
                  name="website"
                  type="text"
                  value={formData.website}
                  onChange={handleChange}
                  required={false}
                />
              </div>
            )}
            {step === 5 && (
              <Question
                label="Which systems do you currently use?"
                name="systems"
                type="multiple-choice"
                value={formData.systems}
                options={[
                  { label: 'WhatsApp', value: 'whatsapp' },
                  { label: 'Slack', value: 'slack' },
                  { label: 'Trello', value: 'trello' },
                  { label: 'Notion', value: 'notion' },
                  { label: 'Google Workspace', value: 'google_workspace' },
                  { label: 'Microsoft Office 365', value: 'office_365' },
                  { label: 'Zoom', value: 'zoom' },
                  { label: 'Skype', value: 'skype' },
                  { label: 'Dropbox', value: 'dropbox' },
                  { label: 'Asana', value: 'asana' },
                  { label: 'Basecamp', value: 'basecamp' },
                  { label: 'Monday.com', value: 'monday' },
                  { label: 'Salesforce', value: 'salesforce' },
                  { label: 'HubSpot', value: 'hubspot' },
                  { label: 'Mailchimp', value: 'mailchimp' },
                  { label: 'Hootsuite', value: 'hootsuite' },
                  { label: 'Buffer', value: 'buffer' },
                  { label: 'QuickBooks', value: 'quickbooks' },
                  { label: 'Xero', value: 'xero' },
                  { label: 'Wave', value: 'wave' },
                  { label: 'Stripe', value: 'stripe' },
                  { label: 'PayPal', value: 'paypal' },
                ]}
                onChange={handleChange}
                required={false}
              />
            )}
            {step === 6 && (
              <Question
                label="Where do you have most of your data?"
                name="data_location"
                type="multiple-choice"
                value={formData.data_location}
                options={[
                  { label: 'Shopify', value: 'shopify_database' },
                  { label: 'Notion', value: 'notion' },
                  { label: 'Google Sheets', value: 'google_sheets' },
                  { label: 'Excel', value: 'excel' },
                  { label: 'AWS RDS', value: 'rds' },
                  { label: 'Microsoft SQL Server', value: 'sql_server' },
                  { label: 'MySQL', value: 'mysql' },
                  { label: 'PostgreSQL', value: 'postgresql' },
                  { label: 'MongoDB', value: 'mongodb' },
                  { label: 'Dropbox', value: 'dropbox' },
                  { label: 'Confluence', value: 'confluence' },
                  { label: 'Google Drive', value: 'google_drive' },
                  { label: 'OneDrive', value: 'onedrive' },
                  { label: 'Box', value: 'box' },
                  { label: 'Other', value: 'other' },
                ]}
                onChange={handleChange}
                required={false}
              />
            )}
            {step === 7 && (
              <div className="mt-32">
                <Question
                  label="Any current pain points or bottlenecks you are facing that you like to share?"
                  name="painPoints"
                  type="textarea"
                  value={formData.painPoints}
                  onChange={handleChange}
                  required={false}
                />
              </div>
            )}
          </div>
        )}
        {!formSubmitted && (
          <div className="text-center mt-4 flex gap-2 justify-center fixed bottom-8 sm:relative sm:mt-10 w-full px-5">
            {step > 0 && (
              <button onClick={handlePrev} className="py-3 w-16 bg-white text-black rounded text-2xl md:text-xl">
                <svg className="mx-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="42" height="42">
                  <path fill="currentColor" d="M15.41 16.59L10.83 12l4.58-4.59L14 6l-6 6 6 6z"/>
                </svg>
              </button>
            )}
              <button onClick={handleNext} className="px-10 py-3 flex-grow bg-white text-black rounded text-2xl md:text-xl md:w-40 md:flex-grow-0">
                {step === 0 ? 'Start' : step < totalSteps ? 'Next' : 'Send'}
              </button>
          </div>
        )}
        {formSubmitted && (
          <div className="text-center mt-32 px-6">
            <h1 className="text-4xl">Finally, feel free to book a kick-off call with us.</h1>
            {customerId !== null && customerId && (
              <CalendlyWidget customerId={customerId}></CalendlyWidget>
            )}
          </div>
        )}
      </main>
    </div>
  );
};

export default Home;
