import React, { useRef } from 'react';
import Image from "next/image";

import Header from "@/components/header";
import Thunders from "@/components/thunders";
import Head from 'next/head';

import router from 'next/router';

export default function Home() {

  const thunderContainerRef = useRef<HTMLDivElement>(null);
  const currentYear = new Date().getFullYear();

  function navigateToBuyPage() {
    router.push('/ai-report-form');
  }

  return (
  <div className="main bg-black text-white">
    <Head>
      <title>Neun - AI Adoption for Small Businesses</title>
      <meta name="description" content="NEUN - AI Adoption for Small Businesses" />
    </Head>
    <Header></Header>
    <div className="hero mt-48 md:mt-40 text-center px-10">
      <h1 className="text-4xl font-thin leading-relaxed">AI Adoption Report for Small Businesses</h1>
      <div className="footer text-white text-center p-5 font-thin text-sm w-full">
        <p>contact@neunsoft.com</p>
        <div className="flex justify-center mt-10">
            <a href="#read" className="mx-2 w-40 font-normal px-2 text-md py-3 border border-white rounded text-white hover:bg-white hover:text-black transition duration-300">
                Read More
            </a>
            <a href="#buy" className="mx-2 w-40 font-normal px-2 text-md py-3 border border-white rounded text-white hover:bg-white hover:text-black transition duration-300">
                Get Mine
            </a>
        </div>

        <a href="#read">
          <svg className='mx-auto mt-10 cursor-pointer' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
            <path fill="currentColor" d="M20 10l-8 8-8-8h16z" />
          </svg>
        </a>
      </div>
      <h1 id="read" className="text-4xl py-64 font-thin leading-relaxed">Integrating AI can greatly enhance your daily operations and competitiveness.</h1>
      <h1 className="text-4xl py-40 font-thin leading-relaxed">We offer a personalized AI adoption service that reviews your business and provides a custom report on how to effectively start using and integrating AI.</h1>
      <h1 className="text-4xl py-40 font-thin leading-relaxed">Our reports are based on extensive research, giving you valuable insights and future plans for AI integration.</h1>
      <h1 className="text-4xl py-40 font-thin leading-relaxed">Whether you are a technical or non-technical business leader, our reports will give you key insights to enhance your AI strategy.</h1>
      <h1 className="text-4xl py-40 font-thin leading-relaxed"><b>Who is this service for?</b> Our AI adoption service is designed for small businesses looking to integrate AI into their operations.</h1>
      <h1 className="text-4xl py-40 font-thin leading-relaxed"><b>What is included?</b> A detailed report on AI integration strategies tailored to your business needs, with specific recommendations and future plans.</h1>
      <h1 className="text-4xl pt-20 font-thin leading-relaxed" id="buy">Get your report:</h1>
      <div onClick={navigateToBuyPage} className="mt-20 mb-64 px-2 mx-auto max-w-[600px]">
        <div className="text-center border border-white py-6 px-4 rounded bg-white text-black cursor-pointer">
          <h1 className="text-3xl font-thin leading-relaxed">AI Adoption Report</h1>
          <h1 className="text-3xl font-thin leading-relaxed">$100</h1>
          <div className="includes text-center">
            <h3 className="text-2xl font-thin leading-relaxed">· For small businesses</h3>
          </div>
          <div className="text-center mb-3">
            <button className="border-black bg-white text-black border-2 rounded py-2 mt-4 px-24"> Buy </button>
          </div>
        </div>
      </div>
    </div>
    <footer>
      <div className="footer text-white text-center p-5 font-thin text-sm">
        <p>&copy; {currentYear} NEUN - contact@neunsoft.com</p>
      </div>
    </footer>
  </div>
  );
}
