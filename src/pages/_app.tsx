// pages/_app.tsx (or _app.js for JavaScript)

import type { AppProps } from 'next/app';
import Head from 'next/head';
import '../globals.css'; // Example global styles import

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Neunsoft</title>
        <link rel="icon" href="/favicon.ico" />
        {/* Add any other meta tags or links to stylesheets here */}
      </Head>
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
