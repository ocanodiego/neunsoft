import React, { useRef } from 'react';
import Image from "next/image";

import Header from "@/components/header";
import Thunders from "@/components/thunders";
import Head from 'next/head';

export default function Home() {

  const thunderContainerRef = useRef<HTMLDivElement>(null);
  const currentYear = new Date().getFullYear();

  return (
  <div className="main bg-black text-white">
    <Head>
      <title>Neun - For Startups</title>
      <meta name="description" content="NEUN - For Startups" />
    </Head>
    <Header></Header>
    <div className="hero mt-48 md:mt-40 text-center px-10">
      {/* <Thunders></Thunders> */}
      <h1 className="text-4xl font-thin leading-relaxed"> For Startups</h1>
      <div className="footer text-white text-center p-5 font-thin text-sm w-full">
        <p>contact@neunsoft.com</p>
        <a>
          <svg className='mx-auto mt-10 cursor-pointer' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
            <path fill="currentColor" d="M20 10l-8 8-8-8h16z" />
          </svg>
        </a>
      </div>
      {/* <h2 className="mt-4">Find Out Why</h2> */}
      <div id="jdd">
        <h1 className="text-4xl my-80 font-thin leading-relaxed">&quot;Just don&#39;t die&quot; - Dalton Caldwell</h1>
      </div>
      <h1 className="text-4xl my-80 font-thin leading-relaxed">As a startup, every resource you have is of extreme value.</h1>
      <h1 className="text-4xl my-80 font-thin leading-relaxed">Instead of trying to rip you off, like most development agencies do, we will share key knowledge with you that will give you the ability to get started on the right foot from a technical perspective.</h1>
      <h1 className="text-4xl my-80 font-thin leading-relaxed">This is why instead of providing you a service, we have created comprehensive and extremely helpful guides to help you set up your project and development strategy. So you can start with the right foot, at a fraction of the cost.</h1>
      <h1 className="text-4xl my-80 font-thin leading-relaxed">Our guides are based on our own experience, building docens of systems for clients in very diverse idea spaces and use cases. From enterprise software and internal products to B2B and consumer products. We have extensive experience.</h1>
      
      <h1 className="text-4xl my-80 font-thin leading-relaxed">Wether you are a technical or non-technical cofounder our guides will give you key insights and knowledge that can help you form, polish, validate or cross check your existing plans for building your product.</h1>
      <h1 className="text-4xl my-80 font-thin leading-relaxed"><b>Who are these guides for?</b> We have designed our guides specifically for <b>technical cofounders</b> who have <b>not</b> yet started developing their product. If you are a non-technical cofounder, our guides can still be of extreme help. We recommend you have completed the free <a className='font-bold' target="_blank" href="https://www.startupschool.org/">startupschool.org</a> course by <a target='_blank' className='font-bold' href="https://www.ycombinator.com/">Y Combinator</a>. As our product is based on those principles.</h1>
      <h1 className="text-4xl my-80 font-thin leading-relaxed"><b>What is included?</b> We include the main tech stack and deployment strategy guide, which contains articles with further details and information that support our choices.</h1>
      <h1 className="text-4xl my-80 font-thin leading-relaxed"><b>What should your expectations be?</b> We have not created these guides so you can brag about an extremely sophisticated set up. If you are looking to brag about it you should totally look somewhere else. These guides outline cost effective, quick, efficient, reliable and fairly known ways to get started that are a lot of the times overlooked, and lead early stage startups to waste a lot of resources unnecessarily. They are not fancy, but for sure deliver on what&#39;s important at these early stages of your business.</h1>
      <h1 className="text-4xl my-80 font-thin leading-relaxed"><b>What is NOT included?</b> We don&#39;t work with serverless technologies because we don&#39;t recommend them for early stage startups in most of the cases. We also don&#39;t work with Microsoft products like .NET, C#, Azure, etc. We don&#39;t suggest using identity providers like AWS Cognito, more information and reasons for this are included in the guide.</h1>
      <h1 className="text-4xl mt-80 font-thin leading-relaxed">So if you have followed along and find this interesting please check our prices:</h1>
      <div className="grid md:grid-cols-2 mt-20 mb-80 px-20 gap-5">
        <div className="text-left border border-white py-6 px-10 rounded hover:bg-white hover:text-black cursor-pointer">
          <h1 className="text-3xl font-thin leading-relaxed">A guide to quick iteration $100</h1>
          <div className="includes text-left">
            <h3 className="text-2xl font-thin leading-relaxed">· Effective and quick deployment</h3>
            <h3 className="text-2xl font-thin leading-relaxed">· 3 tech stack options</h3>
          </div>
          <div className="w-full text-center">
            <button className="border-black bg-white text-black border-2 rounded py-2 mt-4 px-24"> Buy </button>
          </div>
        </div>
        <div className="text-left border border-white py-6 px-10 rounded hover:bg-white hover:text-black cursor-pointer mt-10 md:mt-0">
          <h1 className="text-3xl font-thin leading-relaxed">A guide to hiring remotely $50</h1>
          <div className="includes text-left">
            <h3 className="text-2xl font-thin leading-relaxed">· Effective and quick deployment</h3>
            <h3 className="text-2xl font-thin leading-relaxed">· 3 tech stack options</h3>
          </div>
          <div className="w-full text-center">
            <button className="border-black bg-white text-black border-2 rounded py-2 mt-4 px-24"> Buy </button>
          </div>
        </div>
       
      </div>

    </div>
    <footer>
      <div className="footer text-white text-center p-5 font-thin text-sm">
        <p>&copy; {currentYear} NEUN - contact@neunsoft.com</p>
      </div>
    </footer>
  </div>
  );
}
