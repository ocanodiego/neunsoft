'use client';

import {
  NavigationMenu,
  NavigationMenuContent,
  NavigationMenuIndicator,
  NavigationMenuItem,
  NavigationMenuLink,
  NavigationMenuList,
  NavigationMenuTrigger,
  NavigationMenuViewport,
} from "@/components/ui/navigation-menu"

export default function Header(){
  return (
    <header className="div px-8 py-6 md:p-5 text-white flex justify-between align-center">
      <div className="logo text-center w-full">
        <h1 className="text-4xl">NEUN</h1>
      </div>
    </header>
  );
}