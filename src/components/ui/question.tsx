import React, { useEffect, useRef } from 'react';

type QuestionType = 'text' | 'textarea' | 'multiple-choice' | 'select' | 'calendar';

interface Option {
  label: string;
  value: string;
}

interface QuestionProps {
  label: string;
  name: string;
  type: QuestionType;
  value: string;
  options?: Option[];
  onChange: (name: string, value: string) => void;
  required?: boolean;
}

const Question: React.FC<QuestionProps> = ({
  label,
  name,
  type,
  value,
  options,
  onChange,
  required = false,
}) => {
  const [error, setError] = React.useState('');
  const inputRef = useRef<HTMLInputElement>(null);
  const textareaRef = useRef<HTMLTextAreaElement>(null);
  const selectRef = useRef<HTMLSelectElement>(null);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => {
    onChange(name, e.target.value);
  };

  const handleCheckboxChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value: optionValue, checked } = e.target;
    const currentValues = value ? value.split(',') : [];
    let newValues = [];
    if (checked) {
      newValues = [...currentValues, optionValue];
    } else {
      newValues = currentValues.filter((v) => v !== optionValue);
    }
    onChange(name, newValues.join(','));
  };

  useEffect(() => {
    const handleKeyDown = (e: KeyboardEvent) => {
      if (e.key === 'Enter' && !required) {
        setError('');
      } else if (e.key === 'Enter' && required && !value) {
        setError('This field is required');
      }
    };
    document.addEventListener('keydown', handleKeyDown);
    return () => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [value, required]);

  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    } else if (textareaRef.current) {
      textareaRef.current.focus();
    } else if (selectRef.current) {
      selectRef.current.focus();
    }
  }, []);

  return (
    <div className="text-left md:text-center">
      <h2 className="text-3xl mb-8" dangerouslySetInnerHTML={{ __html: label }}></h2>
      {type === 'text' && (
        <input
          ref={inputRef}
          type="text"
          name={name}
          value={value}
          onChange={handleChange}
          className="w-full px-3 py-2 text-2xl text-black rounded mb-8 focus:outline-none"
          required={required}
        />
      )}
      {type === 'textarea' && (
        <textarea
          ref={textareaRef}
          name={name}
          value={value}
          onChange={handleChange}
          className="w-full px-3 py-2 text-2xl text-black rounded mb-8 focus:outline-none"
          rows={4}
          required={required}
        ></textarea>
      )}
      {type === 'multiple-choice' && options && (
        <div className="mb-8">
          {options.map((option) => (
            <label key={option.value} className="mr-2 text-2xl border px-3 inline-block py-1 mb-2">
              <input
                type="checkbox"
                name={name}
                value={option.value}
                checked={value.split(',').includes(option.value)}
                onChange={handleCheckboxChange}
                className="mr-2"
                required={required}
              />
              {option.label}
            </label>
          ))}
        </div>
      )}
      {type === 'select' && options && (
        <select
          ref={selectRef}
          name={name}
          value={value}
          onChange={handleChange}
          className="w-full p-2 text-black rounded mb-8"
          required={required}
        >
          {options.map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </select>
      )}
      {type === 'calendar' && (
        <input
          ref={inputRef}
          type="date"
          name={name}
          value={value}
          onChange={handleChange}
          className="w-full p-2 text-black rounded mb-8"
          required={required}
        />
      )}
      {error && <div className="text-red-500 mb-8">{error}</div>}
    </div>
  );
};

export default Question;