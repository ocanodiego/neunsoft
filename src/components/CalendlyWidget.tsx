import React, { useEffect } from 'react';
import Head from 'next/head';

interface Calendly {
  initPopupWidget(options: { url: string }): void;
}

declare global {
  interface Window {
    Calendly?: Calendly;
  }
}
interface CalendlyLinkWidgetProps {
  customerId: string;
}


const CalendlyLinkWidget: React.FC<CalendlyLinkWidgetProps> = ({ customerId }) => {
  useEffect(() => {
    const handleCalendlyPopup = (e: MouseEvent) => {
      e.preventDefault();
      if (window.Calendly) {
        const calendlyUrl = `https://calendly.com/poncianodiego/ai-integration-kick-off-meeting?a1=${customerId}`;
        window.Calendly.initPopupWidget({ url: calendlyUrl });
      }
    };

    document.getElementById('calendly-link')?.addEventListener('click', handleCalendlyPopup);

    return () => {
      document.getElementById('calendly-link')?.removeEventListener('click', handleCalendlyPopup);
    };
  }, [customerId]);

  return (
    <>
      <Head>
        <link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet" />
        <script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript" async></script>
      </Head>
      {customerId && (
        <a id="calendly-link" href="#" className="py-3 mx-10 bg-white text-black rounded text-2xl md:text-xl px-8 mt-10 block">
          Schedule a call
        </a>
      )}
    </>
  );
};

export default CalendlyLinkWidget;
