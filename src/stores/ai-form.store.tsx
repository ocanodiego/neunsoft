import {create, StateCreator } from 'zustand';
import { persist, PersistOptions } from 'zustand/middleware';

interface FormData {
  facebook: string;
  instagram: string;
  linkedin: string;
  website: string;
  systems: string;
  otherSystems: string;
  painPoints: string;
  data_location: string;
}

interface FormState {
  step: number;
  formData: FormData;
  nextStep: () => void;
  prevStep: () => void;
  updateFormData: (data: Partial<FormData>) => void;
}

const initialFormData: FormData = {
  facebook: '',
  instagram: '',
  linkedin: '',
  website: '',
  systems: '',
  otherSystems: '',
  painPoints: '',
  data_location: '',
};

type MyPersist = (
  config: StateCreator<FormState>,
  options: PersistOptions<FormState>
) => StateCreator<FormState>;

const useFormStore = create<FormState>(
  (persist as MyPersist)(
    (set) => ({
      step: 0,
      formData: initialFormData,
      nextStep: () => set((state) => ({ step: state.step + 1 })),
      prevStep: () => set((state) => ({ step: state.step - 1 })),
      updateFormData: (data) => set((state) => ({ formData: { ...state.formData, ...data } })),
    }),
    {
      name: 'form-storage',
    }
  )
);

export default useFormStore;
